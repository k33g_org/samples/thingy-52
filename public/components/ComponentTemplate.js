import {Style, Template, Ossicle} from '../js/Ossicle.js'


const template = Template.html`

`

class ComponentTemplate extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {}
}

customElements.define('component-template', ComponentTemplate)
