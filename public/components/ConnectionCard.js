import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './ConsoleLog.js'
import './bulma/BulmaCard.js'
import './tools/MarkDownCard.js'

import {Thingy52} from './Thingy52.js'

const template = Template.html`
  <bulma-card title="Connection & Temperature">
    <!-- UI -->
    <div class="field is-grouped">
      <p class="control">
        <button name="btnConnect" class="button is-dark">🚀 Connect</button>
      </p>
      <p class="control">
        <button name="btnTemperature" class="button is-primary">Temperature</button>
      </p>
      <p class="control">
        <button name="btnDisconnect" class="button is-dark">💥 Disconnect</button>
      </p>
    </div>
    <!-- console -->
    <console-log color="is-primary" title="📟 console"></console-log>

    <hr>
    <mark-down-card file="./components/ConnectionCard.md"></mark-down-card>
  </bulma-card> <!-- card -->
`

class ConnectionCard extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  async connect(device) {
    try {
      let info = await device.connect()
      console.log(info)
      let deviceName = await device.name.read()
      let battery = await device.battery.read()
      console.log(deviceName, battery)
      let status = info === true ? "connected" : "not connected"
      this.console.log(`😀 ${info}: ${deviceName.name} is ${status} | battery: ${battery.status}%`)
    } catch (error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  async disconnect(device) {
    try {
      await device.disconnect()
      this.console.log(`👋 device is disconnected`)
    } catch (error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  async temperature(device) {
    try {
      device.addEventListener("temperature", data => {
        //console.log(data.detail)
        this.console.clear()
          .log(`😀 ${JSON.stringify(data.detail)}`)
          .log(`🌡 Temperature: ${data.detail.value} ${data.detail.unit}`)
      })
      await device.temperature.start()
    } catch(error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  connectedCallback() {
    this.console = this.$("console-log")

    this.$name("btnConnect").addEventListener("click", async () => {
      this.connect(Thingy52.device)
    })

    this.$name("btnTemperature").addEventListener("click", async _ => {
      this.temperature(Thingy52.device)
    })

    this.$name("btnDisconnect").addEventListener("click", async () => {
      this.disconnect(Thingy52.device)
    })

  }
}

customElements.define('connection-card', ConnectionCard)
