## Connection & Temperature

### Initialize

```javascript
const thingy = new Thingy({logEnabled: true})
```

### Connect

```javascript
async connect(device) {
  try {
    let info = await device.connect()
    let deviceName = await device.name.read()
    let status = info === true ? "connected" : "not connected"
    console.log(`😀 ${info}: ${deviceName.name} is ${status}`)
  } catch (error) {
    console.log(`😡 ${error}`)
  }
}

async connect(thingy)

// use `await device.disconnect()` to disconnect
```

### Temperature

```javascript
async temperature(device) {
  try {
    device.addEventListener("temperature", data => {
      //console.log(`😀 ${JSON.stringify(data.detail)}`)
      console.log(`🌡 Temperature: ${data.detail.value} ${data.detail.unit}`)
    })
    await device.temperature.start()
  } catch(error) {
    console.error(`😡 ${error}`)
  }
}

async temperature(thingy)
```
