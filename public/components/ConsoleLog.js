import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './bulma/BulmaMessage.js'

const template = Template.html`
  <bulma-message title="" color="">
  </bulma-message>
`

class ConsoleLog extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.messageBox = this.$("bulma-message")
    this.messageBox.title = this.getAttribute("title")
    this.messageBox.color = this.getAttribute("color")
  }

  log(message) {
    this.messageBox.output+=`${message}\n`
    return this
  }

  clear() {
    this.messageBox.output=``
    return this
  }

}

customElements.define('console-log', ConsoleLog)

console.log("🤖 > loading ConsoleLog component")
