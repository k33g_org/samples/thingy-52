import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './ConsoleLog.js'
import './bulma/BulmaCard.js'
import './tools/MarkDownCard.js'

import {Thingy52} from './Thingy52.js'

const template = Template.html`
  <bulma-card title="LED & Information">
    <!-- UI -->
    <div class="field is-grouped">
      <p class="control">
        <button name="btnLedInformation" class="button is-primary">LED Information</button>
      </p>
      <p class="control">
        <button name="btnLedBreatheBlue" class="button is-primary">Breathe 🔵</button>
      </p>
      <p class="control">
        <button name="btnLedBreatheRed" class="button is-primary">Breathe 🔴</button>
      </p>
      <p class="control">
        <button name="btnLedBreatheGreen" class="button is-primary">Breathe 🟢</button>
      </p>
      <p class="control">
        <button name="btnLedBreatheWhite" class="button is-primary">Breathe ⚪️</button>
      </p>
    </div>
    <!-- console -->
    <console-log color="is-warning" title="📟 console"></console-log>

    <hr>
    <mark-down-card file="./components/LedCard.md"></mark-down-card>

  </bulma-card> <!-- card -->
`

class LedCard extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  async information(device) {
    try {
      let currentLedConfiguration = await device.led.read()
      console.log(currentLedConfiguration)
      this.console.clear().log(`😀 LED info: ${JSON.stringify(currentLedConfiguration)}`)
    } catch (error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  async breathe(device, color, intensity, delay) {
    try {
      const newLedConfiguration = {
          mode: "breathe",
          color: color,
          intensity: intensity,
          delay: delay
      }

      await device.led.write(newLedConfiguration)
      this.console
        .log(`💡 New LED configuration: ${JSON.stringify(newLedConfiguration)}`)

    } catch(error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  connectedCallback() {
    this.console = this.$("console-log")

    this.$name("btnLedInformation").addEventListener("click", async () => {
      this.information(Thingy52.device)
    })

    this.$name("btnLedBreatheBlue").addEventListener("click", async _ => {
      this.breathe(Thingy52.device, "blue", 50, 1000)
    })
    this.$name("btnLedBreatheRed").addEventListener("click", async _ => {
      this.breathe(Thingy52.device, "red", 50, 500)
    })
    this.$name("btnLedBreatheGreen").addEventListener("click", async _ => {
      this.breathe(Thingy52.device, "green", 50, 1000)
    })
    this.$name("btnLedBreatheWhite").addEventListener("click", async _ => {
      this.breathe(Thingy52.device, "white", 100, 50)
    })

  }
}

customElements.define('led-card', LedCard)
