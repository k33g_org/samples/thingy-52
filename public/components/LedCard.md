## LED

### Information

```javascript
async information(device) {
  try {
    let currentLedConfiguration = await device.led.read()
    console.log(`😀 LED info: ${JSON.stringify(currentLedConfiguration)}`)
  } catch (error) {
    console.error(`😡 ${error}`)
  }
}

async information(thingy)
```


### Breathe

```javascript
async breathe(device) {
  try {
    const newLedConfiguration = {
        mode: "breathe",
        color: "cyan",
        intensity: 50,
        delay: 1000,
    }

    await device.led.write(newLedConfiguration)
    console.log(
      `💡 New LED configuration: ${JSON.stringify(newLedConfiguration)}`
    )

  } catch(error) {
    console.error(`😡 ${error}`)
  }
}

async breathe(thingy)
```
