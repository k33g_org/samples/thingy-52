import {Style, Template, Ossicle, Router} from '../js/Ossicle.js'
import './MainTitle.js'

import './ConnectionCard.js'
import './LedCard.js'
import './SoundCard.js'


const template = Template.html`
<div>
  <main-title></main-title>

  <div class="container">

    <section id="" class="section">

        <!-- === Columns === -->

        <div class="columns">

          <div class="column">
            <connection-card></connection-card>
          </div> <!-- column -->

          <div class="column">
            <led-card></led-card>
          </div> <!-- column -->

        </div><!-- columns -->

        <!-- === Columns === -->

        <div class="columns">

          <div class="column">
            <sound-card></sound-card>
          </div> <!-- column -->

        </div><!-- columns -->

    </section>

    <section id="" class="section">

    </section>

    <section id="" class="section">
    </section>
  </div>

</div>
`

class MainApplication extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {}

}

customElements.define('main-application', MainApplication)
