import {Style, Template, Ossicle} from '../js/Ossicle.js'

const mainTitle = "Thingy:52"
const subTitle = "🧪 experiments"

const template = Template.html`
  <section class="hero is-bold is-dark">
    <div class="hero-body">
      <div class="container">
        <h1 class="title is-1">
          ${mainTitle}
        </h1>
        <h2 class="subtitle">
          ${subTitle}
        </h2>
      </div>
    </div>
  </section>
`

class MainTitle extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {}
}

customElements.define('main-title', MainTitle)
