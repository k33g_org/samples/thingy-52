import {Style, Template, Ossicle} from '../js/Ossicle.js'
import './ConsoleLog.js'
import './bulma/BulmaCard.js'
import './tools/MarkDownCard.js'

import {Thingy52} from './Thingy52.js'

/*
First, set the speaker to the right mode

await thingy.soundconfiguration.write({speakerMode: 3});

Then the feature can be written to like this

await thingy.speakerdata.write({"mode":3,"sample":5})

*/



const template = Template.html`
  <bulma-card title="Sound(s)">
    <!-- UI -->
    <div class="field is-grouped">
      <p class="control">
        <button name="btnSound1" class="button is-primary">Sound 1</button>
      </p>
      <p class="control">
        <button name="btnSound2" class="button is-primary">Sound 2</button>
      </p>
    </div>
    <!-- console -->
    <console-log color="is-primary" title="📟 console"></console-log>

    <hr>
    <mark-down-card file="./components/SoundCard.md"></mark-down-card>
  </bulma-card> <!-- card -->
`

class SoundCard extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  async sound(device, sample) {
    try {
      let config = await device.soundconfiguration.write({speakerMode: 3})
      let data = await device.speakerdata.write({"mode":3,"sample": sample})
      this.console.clear().log(`📝 ${config}`).log(`🔈 ${data}`)

    } catch(error) {
      console.error(error)
      this.console.log(`😡 ${error}`)
    }
  }

  connectedCallback() {
    this.console = this.$("console-log")


    this.$name("btnSound1").addEventListener("click", async _ => {
      this.sound(Thingy52.device, 5)
    })

    this.$name("btnSound2").addEventListener("click", async _ => {
      this.sound(Thingy52.device, 6)
    })


  }
}

customElements.define('sound-card', SoundCard)
