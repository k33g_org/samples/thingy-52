import Thingy from "../node_modules/thingy52_web_bluetooth/js/Thingy.js"

const thingy = new Thingy({logEnabled: true})

let Thingy52 = {
  device: thingy
}

export {Thingy52}

console.log("🤖 > loading Thingy52")
