import {Style, Template, Ossicle} from '../../js/Ossicle.js'

const template = Template.html`
  <div class="card">
    <header class="card-header">
      <p class="card-header-title" style="height:70px">
        <!-- title -->
      </p>
    </header>
    <div class="card-content">
      <div class="content">
        <slot></slot>
      </div> <!-- content -->
    </div> <!-- card-content -->
  </div> <!-- card -->
`

class BulmaCard extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {
    this.$class("card-header-title").innerText = this.getAttribute("title")
  }
}

customElements.define('bulma-card', BulmaCard)
