import {Style, Template, Ossicle} from '../../js/Ossicle.js'

const template = Template.html`
  <article class="message">
    <div class="message-header">
      <p><!--title--></p>
      <!--
      <button class="delete" aria-label="delete"></button>
      -->
    </div>
    <div class="message-body">
      <slot></slot>
    </div>
  </article>
`

class BulmaMessage extends Ossicle {

  set title(value) {
    this.$("p").innerText = value
  }

  set color(value) {
    this.$("article").className += " " + value
  }

  set output(value) {
    this.$class("message-body").innerText = value
  }
  get output() {
    return this.$class("message-body").innerText
  }

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  connectedCallback() {

  }


}

customElements.define('bulma-message', BulmaMessage)
