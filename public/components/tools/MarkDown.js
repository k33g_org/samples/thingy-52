import {Style, Template, Ossicle} from '../../js/Ossicle.js'


const sheetCode = Style.css`
  code {
    font-family: Menlo;
    border-radius: 8px;
  }
  p {
    font-size:1.1em;
  }
`

const template = Template.html`
  <content>
    <h1>⏳ loading...</h1>
  </content>
`

class MarkDown extends Ossicle {

  constructor() {
    super()

    this.setup(template.clone(), [
      Style.sheets.highlight,
      Style.sheets.purple,
      sheetCode
    ])
  }

  initialize(value) {
    fetch(value)
      .then(response => response.text())
      .then(mdText => {
        this.$("content").innerHTML = window.markdownit().render(mdText)
        this.$all('code').forEach(code => {
          hljs.highlightBlock(code)
        })
      })
  }

}

customElements.define('mark-down', MarkDown)


