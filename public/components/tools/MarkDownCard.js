import {Style, Template, Ossicle} from '../../js/Ossicle.js'
import './MarkDown.js'

const template = Template.html`
  <button name="btnShowCode" class="button is-gray">Show code</button>
  <div class="modal"> <!-- is-active -->
    <div class="modal-background"></div>
    <div class="modal-content" style="width: 40%">
      <article class="message is-dark">
        <div class="message-header">
          <p name="boxTitle">Source code</p>
          <button class="delete" aria-label="delete"></button>
        </div>
        <div class="message-body">
          <mark-down></mark-down>
        </div>
      </article>
    </div>
    <button name="btnHideCode" class="modal-close is-large" aria-label="close"></button>
  </div>
`

class MarkDownCard extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [
      Style.sheets.bulma
    ])
  }

  connectedCallback() {

    this.$("mark-down").initialize(this.getAttribute("file"))

    this.$name("btnShowCode").addEventListener("click", _ => {
      this.$class("modal").className+=" is-active"
    })

    this.$name("btnHideCode").addEventListener("click", _ => {
      this.$class("modal").className="modal"
    })

    this.$class("delete").addEventListener("click", _ => {
      this.$class("modal").className="modal"
    })


  }
}

customElements.define('mark-down-card', MarkDownCard)


